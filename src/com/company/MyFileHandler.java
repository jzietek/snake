package com.company;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;

import static java.lang.Integer.parseInt;
import static java.util.stream.Collectors.toMap;

/**
 * File handler class.
 */
public class MyFileHandler {

    /**
     * A constructor that creates an object.
     */
    public MyFileHandler() {
    }

    /**
     * Method that creates a file.
     * @param fileName file name
     */
    public void createFile(String fileName) {
        try {
            File file = new File(fileName);
            if (file.createNewFile()) {
                System.out.println("File created: " + file.getName());
            } else {
                System.out.println("File already exists.");
            }
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }

    /**
     * Method that update a file.
     * @param fileName file name
     * @param score    score
     * @param name     name of the player
     */
    public void updateFile(String fileName, int score, String name) {

        Map<String, Integer> data = new HashMap<>();
        data.put(name + "  " + getCurrentDate(), score);

        readFile(fileName, data);
        Map<String, Integer> sortData = sortMap(data);
        try {
            FileWriter writer = new FileWriter(fileName);
            for (int i = 0; i < Math.min(data.size(), 10); i++) {
                Object key = sortData.keySet().toArray()[i];
                Object valueForKey = sortData.get(key);
                writer.write(key + "," + valueForKey);
                writer.write('\n');
            }
            writer.close();
            System.out.println("Successfully wrote to the file.");
        } catch (IOException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }
    }

    /**
     * Method that reads file.
     * @param fileName file name
     * @param data     data
     */
    public void readFile(String fileName, Map<String, Integer> data) {

        try {
            File file = new File(fileName);
            Scanner reader = new Scanner(file);
            while (reader.hasNextLine()) {
                String[] parts = reader.nextLine().split(",", 2);
                data.put(parts[0], parseInt(parts[1]));
            }
            reader.close();
        } catch (FileNotFoundException e) {
            System.out.println("An error occurred.");
            e.printStackTrace();
        }

    }

    /**
     * Method that gets current date.
     * @return date
     */
    private String getCurrentDate() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
        LocalDateTime now = LocalDateTime.now();
        return dtf.format(now);
    }

    /**
     * Method that sorts map.
     * @param data data
     * @return sorted data
     */
    public Map<String, Integer> sortMap(Map<String, Integer> data) {
        return data
                .entrySet()
                .stream()
                .sorted(Collections.reverseOrder(Map.Entry.comparingByValue()))
                .collect(
                        toMap(Map.Entry::getKey, Map.Entry::getValue, (e1, e2) -> e2,
                                LinkedHashMap::new));
    }

    /**
     * Method checks if file exist.
     * @param pathName path name
     * @return true or false
     */
    public static boolean isFileExist(String pathName){
        File f = new File(pathName);
        if(f.exists()){
        return true;
        }
        else{
            return false;
        }
    }
}
