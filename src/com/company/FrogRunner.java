package com.company;

import com.company.gameObjects.Frog;
import com.company.gameObjects.snake.Snake;

/**
 * Frog runner class.
 */
public class FrogRunner extends Thread {

    /**
     * Frog.
     */
    private Frog frog;

    /**
     * A constructor that creates an object.
     * @param frog
     */
    public FrogRunner(Frog frog){
        this.frog = frog;
    }

    /**
     * Method that overrides run method.
     */
    @Override
    public void run() {
        frog.aiMove();
    }

}
