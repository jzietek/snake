package com.company;

import com.company.gameScreens.WelcomeFrame;

/**
 * Main class.
 */
public class Main {

    /**
     * Welcome frame.
     * @param args
     */
    public static void main(String[] args) {

        WelcomeFrame welcomeFrame = new WelcomeFrame("Welcome in Snake Game");
    }
}
