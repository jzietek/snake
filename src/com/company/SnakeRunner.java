package com.company;

import com.company.gameObjects.snake.AISnake;
import com.company.gameObjects.snake.Snake;

/**
 * Snake runner class.
 */
public class SnakeRunner extends Thread{

    /**
     * Snake.
     */
    private Snake snake;

    /**
     * A constructor that creates an object.
     * @param snake
     */
    public SnakeRunner(Snake snake){
        this.snake = snake;
    }

    /**
     * Method that overrides run method.
     */
    @Override
    public void run() {
        snake.snakeMove();
    }
}
