package com.company.gameScreens;

import com.company.*;
import com.company.gameObjects.Frog;
import com.company.gameObjects.ObstacleGenerator;
import com.company.gameObjects.snake.AISnake;
import com.company.gameObjects.FruitGenerator;
import com.company.gameObjects.snake.Snake;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.List;

import java.util.concurrent.TimeUnit;

/**
 * A game panel's class.
 */
public class GamePanel extends JPanel implements ActionListener {

    /**
     * Width of the window.
     */
    public static final int WIDTH_WINDOW = 700;

    /**
     * Height of the window.
     */
    public static final int HEIGHT_WINDOW = 700;

    /**
     * Size of the unit.
     */
    public static final int SIZE_OF_UNIT = 20;

    /**
     * Delay.
     */
    static final int DELAY = 75;

    /**
     * A list of fruits coordinates.
     */
    List<List<Integer>> fruitsCoordinates;

    /**
     * A list of obstacles.
     */
    List<List<Integer>> obstacles;

    /**
     * A list of objects to avoid.
     */
    List<List<Integer>> objectsToAvoid;

    /**
     * Timer.
     */
    Timer timer;

    /**
     * Info about running.
     */
    boolean isRunning = false;

    /**
     * Info about is game started.
     */
    boolean started = false;

    /**
     * Snake.
     */
    public Snake mySnake = new Snake(0, 0, Color.white, obstacles);

    /**
     * Ai Snake.
     */
    AISnake aiSnake;

    /**
     * Frog.
     */
    Frog frog = new Frog(mySnake,aiSnake,obstacles,this);

    /**
     * Obstacle generator.
     */
    ObstacleGenerator obstacleGenerator = new ObstacleGenerator();

    /**
     * Fruit generator.
     */
    FruitGenerator fruitGenerator;

    /**
     * A constructor that creates an object with proper parameters.
     */
    GamePanel() {
        this.setBackground(new Color(184, 3, 255));
        this.setPreferredSize(new Dimension(WIDTH_WINDOW, HEIGHT_WINDOW));
        this.setFocusable(true);
        this.addKeyListener(new MyKeyAdapter());
        startGame();
    }

    /**
     * Method that starts game.
     */
    public void startGame() {
        fruitsCoordinates = new ArrayList<>();
        obstacleGenerator = new ObstacleGenerator();
        obstacles = obstacleGenerator.getObstacles();
        objectsToAvoid = new ArrayList<>();
        generateFruits(3);
        //generateObstacles(1);
        mySnake = new Snake(0, 0, Color.white, obstacles);
        aiSnake = new AISnake((WIDTH_WINDOW - SIZE_OF_UNIT), (HEIGHT_WINDOW - SIZE_OF_UNIT)/2 , Color.RED, obstacles, fruitsCoordinates, mySnake);
        frog = new Frog(mySnake,aiSnake,obstacles,this);
        isRunning = true;
        timer = new Timer(DELAY, this); // how fast snake is running
        timer.start();
    }

    /**
     * Method that paints component.
     * @param g the <code>Graphics</code> object to protect
     */
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        draw(g);
    }

    /**
     * Draw method.
     * @param g
     */
    public void draw(Graphics g) {
        if (!started) {
            drawMessageInTheMiddle(g, Color.black, new Font("Agency FB", Font.BOLD, 60), "Press Space To Start! ");
        }

        if (isRunning && started) {

            obstacleGenerator.draw(g);
            drawMessageInTheMiddle(g, new Color(135, 3, 255), new Font("Agency FB", Font.ITALIC, 100), "Score: " + mySnake.getScore());
            FruitGenerator fruitGenerator = new FruitGenerator(this, fruitsCoordinates, mySnake.getSnakeCoordinates(), obstacles);
            fruitGenerator.draw(g);
            aiSnake.draw(g);
            mySnake.draw(g);
            frog.draw(g);


        } else if (!isRunning && started) {

            drawMessageInTheMiddle(g, Color.black, new Font("Agency FB", Font.BOLD, 125), "Game over");
        }
    }

    /**
     * Method that draws a message.
     * @param g graphics
     * @param c color
     * @param f font
     * @param s message
     */
    private void drawMessageInTheMiddle(Graphics g, Color c, Font f, String s) {
        g.setColor(c);
        g.setFont(f);
        FontMetrics metrics = getFontMetrics(g.getFont());
        g.drawString(s, (WIDTH_WINDOW - metrics.stringWidth(s)) / 2, HEIGHT_WINDOW / 2);
    }

    /**
     * Method that generates fruits.
     * @param amount amount of fruits to be generated
     */
    public void generateFruits(int amount) {
        for (int i = 0; i < amount; i++) {
            generateFruit();
        }
    }

    /**
     * Method that generate a fruit.
     */
    public void generateFruit() {

        fruitsCoordinates.add(generateFruitCoordinates());
    }

    /**
     * Method that generates fruit coordinates.
     * @return coordinates
     */
    public List<Integer> generateFruitCoordinates() {

        List<Integer> fruitCoordinates;
        FruitGenerator fruitGenerator = new FruitGenerator(this, fruitsCoordinates, mySnake.getSnakeCoordinates(), obstacles);
        fruitGenerator.start();
        try {
            fruitGenerator.join();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        fruitCoordinates = fruitGenerator.getFruitCoordinates();
        return fruitCoordinates;
    }

    /**
     * Method that checks if fruit is caught.
     */
    public void checkIfFruitIsCaught() {
        for (int i = 0; i < fruitsCoordinates.size(); i++) {

            if (mySnake.getSnakeCoordinates().get(0).equals(fruitsCoordinates.get(i))) {
                fruitsCoordinates.set(i, generateFruitCoordinates());
                mySnake.setScore(mySnake.getScore() + 1);
                List<List<Integer>> snakeCoordinates = mySnake.getSnakeCoordinates();
                mySnake.addTail(snakeCoordinates.get(snakeCoordinates.size() - 1).get(0), snakeCoordinates.get(snakeCoordinates.size() - 1).get(1));
                if (mySnake.getScore() % 15 == 0 && mySnake.getScore() != 0) {
                    generateFruit();
                }
            }
            if (aiSnake.getSnakeCoordinates().get(0).equals(fruitsCoordinates.get(i))) {
                fruitsCoordinates.set(i, generateFruitCoordinates());
                aiSnake.setScore(mySnake.getScore() + 1);
                List<List<Integer>> snakeCoordinates = aiSnake.getSnakeCoordinates();
                aiSnake.addTail(snakeCoordinates.get(snakeCoordinates.size() - 1).get(0), snakeCoordinates.get(snakeCoordinates.size() - 1).get(1));
            }
        }
    }

    /**
     * Method that checks if frog is caught.
     */
    public void checkIfFrogIsCaught(){

        if (mySnake.getSnakeCoordinates().get(0).equals(frog.getCoordinates())) {
            frog.changeCoordinatesAfterBeingCaught();
            mySnake.setScore(mySnake.getScore() + 1);
            List<List<Integer>> snakeCoordinates = mySnake.getSnakeCoordinates();
            mySnake.addTail(snakeCoordinates.get(snakeCoordinates.size() - 1).get(0), snakeCoordinates.get(snakeCoordinates.size() - 1).get(1));
            if (mySnake.getScore() % 15 == 0 && mySnake.getScore() != 0) {
                generateFruit();
            }
        }
        if (aiSnake.getSnakeCoordinates().get(0).equals(frog.getCoordinates())) {
            frog.changeCoordinatesAfterBeingCaught();
            aiSnake.setScore(mySnake.getScore() + 1);
            List<List<Integer>> snakeCoordinates = aiSnake.getSnakeCoordinates();
            aiSnake.addTail(snakeCoordinates.get(snakeCoordinates.size() - 1).get(0), snakeCoordinates.get(snakeCoordinates.size() - 1).get(1));
        }

    }

    /**
     * Method that checks if is game over.
     */
    public void checkGameOver() {

        if (mySnake.checkGameOver()) {
            isRunning = false;
        }

        for (List<Integer> aiSnakeCoordinate : aiSnake.getSnakeCoordinates()) {
            if (mySnake.getSnakeCoordinates().get(0).equals(aiSnakeCoordinate)) {
                isRunning = false;
                break;
            }
        }
        if (!isRunning) {
            timer.stop();
            goToSleep(2);

            MyFileHandler myFileHandler = new MyFileHandler();
            myFileHandler.createFile("BestResults.txt");
            myFileHandler.updateFile("BestResults.txt", mySnake.getScore(), getPlayerName());

            displayInfo();
        }
    }

    /**
     * Sleep method.
     * @param sec amount of seconds
     */
    private void goToSleep(int sec){
        try {
            TimeUnit.SECONDS.sleep(sec);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * Method that displays info.
     */
    private void displayInfo(){
        int answer = JOptionPane.showConfirmDialog(this, "Your Score: " + mySnake.getScore() + " Do you want to play again?", "", JOptionPane.YES_NO_OPTION);
        if (answer == 0) {
            startGame();
        }
        if (answer == 1) {
            Window win = SwingUtilities.getWindowAncestor(this);
            win.dispose();
            new WelcomeFrame("Welcome in Snake Game");
        }
    }

    /**
     * Method that gets player's name.
     * @return name
     */
    private String getPlayerName() {
        String name = JOptionPane.showInputDialog("What is your name? ");
        if (name == null) {
            name = "Guest";
        } else if (name.length() == 0) {
            name = "Guest";
        }
        return name;
    }

    /**
     * Method that overrides action performed method.
     * @param e the event to be processed
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        repaint();
        if (isRunning && started) {
            startFrogRunner();
            startSnakeRunner();
            startAiSnakeRunner();
            checkIfFruitIsCaught();
            checkIfFrogIsCaught();
            checkGameOver();

        }
    }

    /**
     * Method that starts frog runner.
     */
    private void startFrogRunner(){
        FrogRunner frogRunner = new FrogRunner(frog);
        frogRunner.start();
        try {
            frogRunner.join();
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Method starts snake runner.
     */
    private void startSnakeRunner(){
        SnakeRunner mySnakeRunner = new SnakeRunner(mySnake);
        mySnakeRunner.start();
        try {
            mySnakeRunner.join();
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }
    }

    /**
     * Method that starts ai snake runner.
     */
    private void startAiSnakeRunner(){
        AiSnakeRunner aiSnakeRunner = new AiSnakeRunner(aiSnake);
        aiSnakeRunner.start();
        try {
            aiSnakeRunner.join();
        } catch (InterruptedException exception) {
            exception.printStackTrace();
        }

    }

    /**
     * My key adapter class.
     */
    public class MyKeyAdapter extends KeyAdapter {

    @Override
    public void keyPressed(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_LEFT:
                if (mySnake.getCurrentDirection() != 'R' && mySnake.getSecureDirection() != 'R') {
                    mySnake.setCurrentDirection('L');
                }
                break;
            case KeyEvent.VK_RIGHT:
                if (mySnake.getCurrentDirection() != 'L' && mySnake.getSecureDirection() != 'L') {
                    mySnake.setCurrentDirection('R');
                }
                break;
            case KeyEvent.VK_DOWN:
                if (mySnake.getCurrentDirection() != 'U' && mySnake.getSecureDirection() != 'U') {
                    mySnake.setCurrentDirection('D');
                }
                break;
            case KeyEvent.VK_UP:
                if (mySnake.getCurrentDirection() != 'D' && mySnake.getSecureDirection() != 'D') {
                    mySnake.setCurrentDirection('U');
                }
                break;
            case KeyEvent.VK_SPACE:
                started = true;
                break;
        }
    }
}
}
