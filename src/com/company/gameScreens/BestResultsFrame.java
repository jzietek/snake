package com.company.gameScreens;

import com.company.MyFileHandler;

import javax.swing.*;
import java.awt.*;
import java.util.HashMap;
import java.util.Map;

/**
 * A class of best results frame,
 */
public class BestResultsFrame extends MyFrame {

    /**
     * A map.
     */
    Map<String, Integer> data = new HashMap<String, Integer>();

    /**
     * A file handler.
     */
    MyFileHandler myFileHandler = new MyFileHandler();

    /**
     * A constructor that creates an object with main label content.
     * @param mainLabelContent content
     */
    BestResultsFrame(String mainLabelContent) {
        super(mainLabelContent);
        myFileHandler.readFile("BestResults.txt", data);
        data = myFileHandler.sortMap(data);
        int iter = 1;
        for (Map.Entry<String, Integer> entry : data.entrySet()) {
            panel2.add(Box.createRigidArea(new Dimension(0, 10)));
            panel2.add(initLabel(iter + ".   " + "Score: " + entry.getValue() + "    " + entry.getKey()));
            iter++;
        }

    }

    /**
     * Method that creates initial label.
     * @param stringResult result
     * @return JLabel object
     */
    public JLabel initLabel(String stringResult) {
        JLabel label = new JLabel();
        label.setText(stringResult);
        label.setFont(new Font("Verdana", Font.PLAIN, 25));
        label.setForeground(Color.black);
        label.setBorder(BorderFactory.createEmptyBorder(0, 30, 0, 0));
        label.setAlignmentX(Component.LEFT_ALIGNMENT);
        label.setAlignmentY(Component.CENTER_ALIGNMENT);
        return label;
    }


}
