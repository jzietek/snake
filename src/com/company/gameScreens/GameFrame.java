package com.company.gameScreens;

import javax.swing.*;

/**
 * A Game Frame's class.
 */
public class GameFrame extends JFrame {

    /**
     * A constructor of game frame.
     */
    GameFrame() {
        this.setTitle("Snake");
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Close app after click X
        this.setVisible(true); //make this visible
        this.setResizable(false);
        GamePanel gamePanel = new GamePanel();
        this.add(gamePanel);
        this.pack();
        this.setLocationRelativeTo(null);
    }
}
