package com.company.gameScreens;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;

/**
 * My button class.
 */
public class MyButton extends JButton {

    /**
     * A contructor of my button.
     * @param name
     */
    MyButton(String name) {

        this.setText(name);
        this.setFocusable(false);
        this.setFont(new Font("Agency FB", Font.BOLD, 45));
        this.setMaximumSize(new Dimension(400, 100));
        this.setAlignmentX(Component.CENTER_ALIGNMENT);
        this.setAlignmentY(Component.CENTER_ALIGNMENT);
        this.setBackground(Color.white);
        this.setBorder(new LineBorder(Color.BLACK));
        UIManager.put("Button.select", Color.GRAY);

        this.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                setBackground(Color.GRAY);
            }

            public void mouseExited(java.awt.event.MouseEvent evt) {
                setBackground(Color.white);
            }
        });


    }
}