package com.company.gameScreens;

import com.company.gameScreens.BestResultsFrame;
import com.company.gameScreens.GameWindow;
import com.company.gameScreens.MyButton;
import com.company.gameScreens.MyFrame;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static com.company.MyFileHandler.isFileExist;

/**
 * Welcome frame class.
 */
public class WelcomeFrame extends MyFrame implements ActionListener {

    /**
     * Start game button.
     */
    MyButton startGameButton = new MyButton("Start Game");

    /**
     * Best results button.
     */
    MyButton bestResultsButton = new MyButton("Best Results");

    /**
     * Exit button.
     */
    MyButton exitButton = new MyButton("Exit");

    /**
     * A constructor that creates an object with content.
     * @param mainLabelContent
     */
    public WelcomeFrame(String mainLabelContent) {
        super(mainLabelContent);

        bestResultsButton.setEnabled(isFileExist("BestResults.txt"));

        startGameButton.addActionListener(this);
        bestResultsButton.addActionListener(this);
        exitButton.addActionListener(this);

        panel2.add(Box.createRigidArea(new Dimension(0, 50)));
        panel2.add(startGameButton);
        panel2.add(Box.createRigidArea(new Dimension(0, 50)));
        panel2.add(bestResultsButton);
        panel2.add(Box.createRigidArea(new Dimension(0, 50)));
        panel2.add(exitButton);

        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // Close app after click X
        revalidate();
    }

    /**
     * Method that overrides action perfomrmed method.
     * @param e the event to be processed
     */
    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == startGameButton) {
            this.dispose();
            GameWindow gameWindow = new GameWindow();
        } else if (e.getSource() == exitButton) {
            int answer = JOptionPane.showConfirmDialog(null, "Are you sure, you want to exit?", "", JOptionPane.YES_NO_CANCEL_OPTION);
            if (answer == 0) {
                System.exit(0);
            }
        } else if (e.getSource() == bestResultsButton) {
            BestResultsFrame bestResultsFrame = new BestResultsFrame("Best Results");
        }
    }
}
