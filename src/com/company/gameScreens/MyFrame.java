package com.company.gameScreens;

import javax.swing.*;
import java.awt.*;

/**
 * My frame's class.
 */
public class MyFrame extends JFrame {

    /**
     * JLabel object.
     */
    JLabel mainLabel = new JLabel("");

    /**
     * JPanel first object.
     */
    public JPanel panel1 = new JPanel();

    /**
     * JPanel second object.
     */
    public JPanel panel2 = new JPanel();

    /**
     * A constructor that creates an object with content.
     * @param mainLabelContent
     */
    MyFrame(String mainLabelContent) {

        mainLabel.setText(mainLabelContent);
        panel1.setBackground(new Color(184, 3, 255));
        panel1.setPreferredSize(new Dimension(100, 170));
        panel1.setLayout(new GridBagLayout());

        panel2.setBackground(new Color(184, 3, 255));
        panel2.setPreferredSize(new Dimension(100, 100));
        panel2.setLayout(new BoxLayout(panel2, BoxLayout.Y_AXIS));

        mainLabel.setForeground(Color.white);
        mainLabel.setFont(new Font("Agency FB", Font.BOLD, 75));
        mainLabel.setVerticalAlignment(SwingConstants.CENTER);
        mainLabel.setHorizontalAlignment(SwingConstants.CENTER);
        mainLabel.setForeground(Color.black);
        panel1.add(mainLabel);

        this.setTitle("Snake");
        this.setSize(700, 700);
        this.setMinimumSize(new Dimension(700, 700));
        this.setVisible(true); //make this visible
        this.getContentPane().setBackground(new Color(184, 3, 255)); // change color of background
        this.add(panel1, BorderLayout.NORTH);
        this.add(panel2, BorderLayout.CENTER);
        this.setLocationRelativeTo(null);
    }

}
