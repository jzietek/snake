package com.company.gameObjects.snake;

import com.company.interfaces.Drawable;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static com.company.gameScreens.GamePanel.SIZE_OF_UNIT;
import static com.company.gameScreens.GamePanel.WIDTH_WINDOW;

/**
 * A snake's class.
 */
public class Snake implements Drawable {

    /**
     * A list of snake's coordinates.
     */
    private List<List<Integer>> snakeCoordinates;

    /**
     * A list of obstacles.
     */
    private final List<List<Integer>> obstacles;

    /**
     * Current direction.
     */
    private char currentDirection;

    /**
     * Secure direction.
     */
    private char secureDirection;

    /**
     * A flag that informs about game over.
     */
    private boolean isGameOver = false;

    /**
     * A color.
     */
    private final Color color;

    /**
     * A score.
     */
    private int score;

    /**
     *  A contructor of snake that creates a snake's object with proper parameters.
     * @param xStartPosition initial x coordinate
     * @param yStartPosition initial y coordinate
     * @param color          color
     * @param obstacles      a list of obstacles
     */
    public Snake(int xStartPosition, int yStartPosition, Color color, List<List<Integer>> obstacles) {
        snakeCoordinates = new ArrayList<>();
        this.obstacles = obstacles;
        currentDirection = 'R';
        secureDirection = 'R';
        initialSnake(xStartPosition, yStartPosition);
        score = 0;
        this.color = color;
    }

    /**
     * Method that gets obstacles.
     * @return obstacles
     */
    public List<List<Integer>> getObstacles() {
        return obstacles;
    }

    /**
     * Method that gets a score.
     * @return a score
     */
    public int getScore() {
        return score;
    }

    /**
     * Method that sets a score.
     * @param score set score
     */
    public void setScore(int score) {
        this.score = score;
    }

    /**
     * Method that gets a snake's coordinates,
     * @return snake's coordinates
     */
    public List<List<Integer>> getSnakeCoordinates() {
        return snakeCoordinates;
    }

    /**
     * Method that gets a current direction
     * @return current direction
     */
    public char getCurrentDirection() {
        return currentDirection;
    }

    /**
     * Method that sets a current direction.
     * @param currentDirection set direction
     */
    public void setCurrentDirection(char currentDirection) {
        this.currentDirection = currentDirection;
    }

    /**
     * Method that gets secured direction.
     * @return secured direction
     */
    public char getSecureDirection() {
        return secureDirection;
    }

    /**
     * Method that sets secured direction.
     * @param secureDirection set direction
     */
    public void setSecureDirection(char secureDirection) {
        this.secureDirection = secureDirection;
    }

    /**
     * Method that initializes a snake
     * @param x initial x coordinates
     * @param y initial y coordinates
     */
    private void initialSnake(int x, int y) {
        for (int i = 0; i < 3; i++) {
            addTail(x, y);
        }
    }

    /**
     * Method that adds a tail.
     * @param x initial x coordinates
     * @param y initial y coordinates
     */
    public void addTail(int x, int y) {
        List<Integer> coordinates = new ArrayList<>(2);
        coordinates.add(x);
        coordinates.add(y);
        snakeCoordinates.add(coordinates);
    }

    /**
     * Method that moves a snake.
     */
    public void snakeMove() {
        //to prevent multiclicking buttons and possibility error
        secureDirection = currentDirection;
        for (int i = snakeCoordinates.size() - 1; i > 0; i--) {
            snakeCoordinates.set(i, new ArrayList<>(snakeCoordinates.get(i - 1)));
        }
        List<Integer> headCoordinates = new ArrayList<>(snakeCoordinates.get(0));
        switch (currentDirection) {
            case 'R' -> {
                headCoordinates.set(0, (headCoordinates.get(0) + SIZE_OF_UNIT));
                snakeCoordinates.set(0, headCoordinates);
            }
            case 'L' -> {
                headCoordinates.set(0, (headCoordinates.get(0) - SIZE_OF_UNIT));
                snakeCoordinates.set(0, headCoordinates);
            }
            case 'U' -> {
                headCoordinates.set(1, (headCoordinates.get(1) - SIZE_OF_UNIT));
                snakeCoordinates.set(0, headCoordinates);
            }
            case 'D' -> {
                headCoordinates.set(1, (headCoordinates.get(1) + SIZE_OF_UNIT));
                snakeCoordinates.set(0, headCoordinates);
            }
        }
    }

    /**
     * Method that draws a snake.
     * @param g
     */
    public void draw(Graphics g) {
        for (int i = 0; i < snakeCoordinates.size(); i++) {
            if (i == 0) {
                g.setColor(Color.black);
            } else {
                g.setColor(color);
            }
            g.fillRect(snakeCoordinates.get(i).get(0), snakeCoordinates.get(i).get(1), SIZE_OF_UNIT, SIZE_OF_UNIT);
        }
    }

    /**
     * Methods that checks game over.
     * @return info about game over
     */
    public boolean checkGameOver() {

        //check collision with body
        for (int i = 1; i < snakeCoordinates.size(); i++) {
            if (snakeCoordinates.get(0).equals(snakeCoordinates.get(i))) {
                isGameOver = true;
                break;
            }
        }
        //check collision with borders
        if (snakeCoordinates.get(0).contains(-SIZE_OF_UNIT) || snakeCoordinates.get(0).contains(WIDTH_WINDOW)) {
            isGameOver = true;
        }
        //check collision with obstacles
        for (List<Integer> obstacle : obstacles) {
            if (getSnakeCoordinates().get(0).equals(obstacle)) {
                isGameOver = true;
                break;
            }
        }
        return isGameOver;
    }

}
