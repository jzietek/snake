package com.company.gameObjects.snake;

import com.company.interfaces.AIMode;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;

import static com.company.gameScreens.GamePanel.*;

/**
 * AiSnake's class that inherits attributes and methods after snake's class.
 */
public class AISnake extends Snake implements AIMode {

    /**
     * Coordinates of fruits.
     */
    final List<List<Integer>> fruitsCoordinates;

    /**
     * A list of objects that should be avoided.
     */
    private List<List<Integer>> objectsToAvoid;

    /**
     * Snake object.
     */
    private Snake snake;

    /**
     * A contructor of AiSnake that creates a snake with proper parameters, direction etc.
     *
     * @param xStartPosition    initial x coordinate
     * @param yStartPosition    initial y coordinate
     * @param color             color
     * @param obstacles         a list of obstacles
     * @param fruitsCoordinates fruits coordinates
     * @param snake             snake
     */
    public AISnake(int xStartPosition, int yStartPosition, Color color, List<List<Integer>> obstacles, List<List<Integer>> fruitsCoordinates, Snake snake) {
        super(xStartPosition, yStartPosition, color, obstacles);
        setCurrentDirection('L');
        setSecureDirection('L');
        this.fruitsCoordinates = fruitsCoordinates;
        this.snake = snake;
    }

    /**
     * Method that overrides move.
     */
    @Override
    public void aiMove() {

        changeDirection(findDirection());

        checkBorders();
        checkObstacles();
        this.snakeMove();
    }

    /**
     * Method that checks obstacles.
     */
    public void checkObstacles() {

        this.objectsToAvoid = new ArrayList<>();
        objectsToAvoid.addAll(getObstacles());
        for (int i = 1; i < getSnakeCoordinates().size(); i++) {
            objectsToAvoid.add(getSnakeCoordinates().get(i));
        }
        for (int i = 0; i < snake.getSnakeCoordinates().size(); i++) {
            objectsToAvoid.add(snake.getSnakeCoordinates().get(i));
        }
        List<Integer> currentSnakeHeadPosition = getSnakeCoordinates().get(0);
        List<Integer> nextSnakeHeadPosition = new ArrayList<>();
        switch (getCurrentDirection()) {
            case 'R':
                if (getSecureDirection() == 'R') {
                    nextSnakeHeadPosition.add(currentSnakeHeadPosition.get(0) + SIZE_OF_UNIT);
                    nextSnakeHeadPosition.add(currentSnakeHeadPosition.get(1));
                    if (objectsToAvoid.contains(nextSnakeHeadPosition)) {
                        changeDirection(chooseDirectionInY());
                    }
                }
                break;
            case 'L':
                if (getSecureDirection() == 'L') {
                    nextSnakeHeadPosition.add(currentSnakeHeadPosition.get(0) - SIZE_OF_UNIT);
                    nextSnakeHeadPosition.add(currentSnakeHeadPosition.get(1));
                    if (objectsToAvoid.contains(nextSnakeHeadPosition)) {
                        changeDirection(chooseDirectionInY());
                    }
                }
                break;
            case 'D':
                if (getSecureDirection() == 'D') {
                    nextSnakeHeadPosition.add(currentSnakeHeadPosition.get(0));
                    nextSnakeHeadPosition.add(currentSnakeHeadPosition.get(1) + SIZE_OF_UNIT);
                    if (objectsToAvoid.contains(nextSnakeHeadPosition)) {
                        changeDirection(chooseDirectionInX());
                    }
                }
                break;
            case 'U':
                if (getSecureDirection() == 'U') {
                    nextSnakeHeadPosition.add(currentSnakeHeadPosition.get(0));
                    nextSnakeHeadPosition.add(currentSnakeHeadPosition.get(1) - SIZE_OF_UNIT);
                    if (objectsToAvoid.contains(nextSnakeHeadPosition)) {
                        changeDirection(chooseDirectionInX());
                    }
                }
                break;
        }
    }

    /**
     * Method checks is there possibility to change a direction.
     *
     * @param direction chcecked direction
     * @return result
     */
    public boolean ifDirectionCanBeChanged(char direction) {

        this.objectsToAvoid = new ArrayList<>();
        objectsToAvoid.addAll(getObstacles());
        for (int i = 1; i < getSnakeCoordinates().size(); i++) {
            objectsToAvoid.add(getSnakeCoordinates().get(i));
        }
        for (int i = 0; i < snake.getSnakeCoordinates().size(); i++) {
            objectsToAvoid.add(snake.getSnakeCoordinates().get(i));
        }
        boolean result = false;
        List<Integer> currentSnakeHeadPosition = getSnakeCoordinates().get(0);
        List<Integer> nextSnakeHeadPosition = new ArrayList<>();
        switch (direction) {
            case 'R':
                nextSnakeHeadPosition.add(currentSnakeHeadPosition.get(0) + SIZE_OF_UNIT);
                nextSnakeHeadPosition.add(currentSnakeHeadPosition.get(1));
                result = !objectsToAvoid.contains(nextSnakeHeadPosition);
                break;
            case 'L':
                nextSnakeHeadPosition.add(currentSnakeHeadPosition.get(0) - SIZE_OF_UNIT);
                nextSnakeHeadPosition.add(currentSnakeHeadPosition.get(1));
                result = !objectsToAvoid.contains(nextSnakeHeadPosition);
                break;
            case 'D':
                nextSnakeHeadPosition.add(currentSnakeHeadPosition.get(0));
                nextSnakeHeadPosition.add(currentSnakeHeadPosition.get(1) + SIZE_OF_UNIT);
                result = !objectsToAvoid.contains(nextSnakeHeadPosition);
                break;
            case 'U':
                nextSnakeHeadPosition.add(currentSnakeHeadPosition.get(0));
                nextSnakeHeadPosition.add(currentSnakeHeadPosition.get(1) - SIZE_OF_UNIT);
                result = !objectsToAvoid.contains(nextSnakeHeadPosition);
                break;
        }
        return result;
    }


    /**
     * Method that checks a potential collision with a border and change direction.
     */
    public void checkBorders() {

        switch (getCurrentDirection()) {
            case 'L':
                if (getSnakeCoordinates().get(0).get(0) == 0) {
                    changeDirection(chooseDirectionInY());
                }
                break;
            case 'R':
                if (getSnakeCoordinates().get(0).get(0) == WIDTH_WINDOW - SIZE_OF_UNIT) {
                    changeDirection(chooseDirectionInY());
                }
                break;
            case 'U':
                if (getSnakeCoordinates().get(0).get(1) == 0) {
                    changeDirection(chooseDirectionInX());
                }
                break;
            case 'D':
                if (getSnakeCoordinates().get(0).get(1) == HEIGHT_WINDOW - SIZE_OF_UNIT) {
                    changeDirection(chooseDirectionInX());
                }
                break;
        }
    }

    /**
     * Method that choose x direction.
     *
     * @return proper direction
     */
    private char chooseDirectionInX() {
        if (getSnakeCoordinates().get(0).get(0) < WIDTH_WINDOW / 2) {
            return 'R';
        } else {
            return 'L';
        }
    }

    /**
     * Method that choose y direction.
     *
     * @return proper direction
     */
    private char chooseDirectionInY() {
        if (getSnakeCoordinates().get(0).get(1) < WIDTH_WINDOW / 2) {
            return 'D';
        } else {
            return 'U';
        }
    }

    /**
     * Method that finds the best way to fruits.
     *
     * @return direction to fruit
     */
    public char findDirection() {
        List<Integer> closestFruitCoordinates = findClosestFruit();
        if (Math.abs(closestFruitCoordinates.get(0) - getSnakeCoordinates().get(0).get(0)) < Math.abs(closestFruitCoordinates.get(1) - getSnakeCoordinates().get(0).get(1))) {
            if (closestFruitCoordinates.get(0) < getSnakeCoordinates().get(0).get(0)) {
                return 'L';
            } else if (closestFruitCoordinates.get(0) > getSnakeCoordinates().get(0).get(0)) {
                return 'R';
            } else {
                if (closestFruitCoordinates.get(1) < getSnakeCoordinates().get(0).get(1)) {
                    return 'U';
                } else if (closestFruitCoordinates.get(1) > getSnakeCoordinates().get(0).get(1)) {
                    return 'D';
                }
            }
        } else {
            if (closestFruitCoordinates.get(1) < getSnakeCoordinates().get(0).get(1)) {
                return 'U';
            } else if (closestFruitCoordinates.get(1) > getSnakeCoordinates().get(0).get(1)) {
                return 'D';
            } else {
                if (closestFruitCoordinates.get(0) < getSnakeCoordinates().get(0).get(0)) {
                    return 'L';
                } else if (closestFruitCoordinates.get(0) > getSnakeCoordinates().get(0).get(0)) {
                    return 'R';
                }
            }
        }
        return getCurrentDirection();
    }

    /**
     * Method that finds the closest fruit.
     *
     * @return coordinates to the closest fruit
     */
    public List<Integer> findClosestFruit() {
        List<Integer> coordinates = new ArrayList<>();
        int minDistance = 100000;
        for (List<Integer> fruitCoordinates : fruitsCoordinates) {
            int distance = (int) Math.sqrt(Math.pow(Math.abs(fruitCoordinates.get(0) - getSnakeCoordinates().get(0).get(0)), 2) + Math.pow(Math.abs(fruitCoordinates.get(1) - getSnakeCoordinates().get(0).get(1)), 2));
            if (distance < minDistance) {
                minDistance = distance;
                coordinates = fruitCoordinates;
            }
        }
        return coordinates;
    }

    /**
     * Method that changes direction.
     */
    public void changeDirection(char wantedDirection) {

        switch (wantedDirection) {
            case 'R':
                if (this.getCurrentDirection() != 'L' && this.getSecureDirection() != 'L' && ifDirectionCanBeChanged(wantedDirection)) {
                    this.setCurrentDirection('R');
                }
                break;
            case 'L':
                if (this.getCurrentDirection() != 'R' && this.getSecureDirection() != 'R' && ifDirectionCanBeChanged(wantedDirection)) {
                    this.setCurrentDirection('L');
                }
                break;
            case 'D':
                if (this.getCurrentDirection() != 'U' && this.getSecureDirection() != 'U' && ifDirectionCanBeChanged(wantedDirection)) {
                    this.setCurrentDirection('D');
                }
                break;
            case 'U':
                if (this.getCurrentDirection() != 'D' && this.getSecureDirection() != 'D' && ifDirectionCanBeChanged(wantedDirection)) {
                    this.setCurrentDirection('U');

                }
                break;
        }
    }
}
