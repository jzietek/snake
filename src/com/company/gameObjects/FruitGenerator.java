package com.company.gameObjects;

import com.company.interfaces.Drawable;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.company.gameScreens.GamePanel.*;

/**
 * A fruit generator class responsible for generating fruits on the map.
 */
public class FruitGenerator extends Thread implements Drawable {

    /**
     * Snake's coordinates.
     */
    private final List<List<Integer>> snakeCoordinates;

    /**
     * Fruit's coordinates.
     */
    private List<Integer> fruitCoordinates;

    /**
     * A list of fruit's coordinates.
     */
    private final List<List<Integer>> fruitsCoordinates;

    /**
     * A list of obstacles.
     */
    private final List<List<Integer>> obstacles;

    /**
     * A component.
     */
    private Component component;

    /**
     * A constructor that creates an object with proper parameters.
     * @param component         component
     * @param fruitsCoordinates fruit's coordinates
     * @param snakeCoordinates  snake's coordinates
     * @param obstacles         a list of obstacles
     */
    public FruitGenerator(Component component, List<List<Integer>> fruitsCoordinates, List<List<Integer>> snakeCoordinates, List<List<Integer>> obstacles) {
        this.fruitsCoordinates = fruitsCoordinates;
        this.snakeCoordinates = snakeCoordinates;
        this.obstacles = obstacles;
    }

    /**
     * Method that overrides a run method.
     */
    @Override
    public void run() {
        boolean incorrectCoordination = true;
        Random random = new Random();
        do {
            int fruitX = random.nextInt(WIDTH_WINDOW / SIZE_OF_UNIT) * SIZE_OF_UNIT;
            int fruitY = random.nextInt(HEIGHT_WINDOW / SIZE_OF_UNIT) * SIZE_OF_UNIT;
            fruitCoordinates = new ArrayList<>(2);
            fruitCoordinates.add(fruitX);
            fruitCoordinates.add(fruitY);
            if (!fruitsCoordinates.contains(fruitCoordinates) && !snakeCoordinates.contains(fruitCoordinates) && !obstacles.contains(fruitCoordinates)) {
                incorrectCoordination = false;
            }
        } while (incorrectCoordination);

    }

    /**
     * Method that gets fruit coordinates.
     * @return coordinates
     */
    public List<Integer> getFruitCoordinates() {

        return this.fruitCoordinates;
    }

    /**
     * Method that overrides a draw method.
     * @param g
     */
    @Override
    public void draw(Graphics g) {
        int iter = 0;
        String[] icons = new String[]{"apple", "bananas", "orange"};
        for (List<Integer> fruitsCoordinate : fruitsCoordinates) {

            ImageIcon icon = new ImageIcon(String.format("images/%s.png", icons[iter++ % 3]));
            icon.paintIcon(component, g, fruitsCoordinate.get(0), fruitsCoordinate.get(1));
        }
    }
}
