package com.company.gameObjects;

import com.company.gameObjects.snake.AISnake;
import com.company.gameObjects.snake.Snake;
import com.company.interfaces.AIMode;
import com.company.interfaces.Drawable;

import javax.swing.*;
import java.awt.*;
import java.sql.SQLOutput;
import java.util.ArrayList;
import java.util.List;

import static com.company.gameScreens.GamePanel.*;


/**
 * Frog's class.
 */
public class Frog implements AIMode, Drawable {

    /**
     * Snake.
     */
    private final Snake snake;

    /**
     * AiSnake.
     */
    private final AISnake aiSnake;

    /**
     * A list of obstacles.
     */
    private final List<List<Integer>> obstacle;

    /**
     * A list of frog's coordinates.
     */
    private final List<Integer> coordinates = new ArrayList<>();

    /**
     * A current direction.
     */
    private char currentDirection;

    /**
     * A secure direction.
     */
    private char secureDirection;

    /**
     * A component.
     */
    private final Component component;

    /**
     * Method that gets coordinates.
     * @return coordinates
     */
    public List<Integer> getCoordinates() {
        return coordinates;
    }

    /**
     * A constructor of a Frog that creates a object with proper parameters.
     * @param snake     snake
     * @param aiSnake   Ai Snake
     * @param obstacle  obstacles
     * @param component component
     */
    public Frog(Snake snake, AISnake aiSnake, List<List<Integer>> obstacle, Component component) {
        this.snake = snake;
        this.aiSnake = aiSnake;
        this.obstacle = obstacle;
        coordinates.add(360);
        coordinates.add(360);
        currentDirection = snake.getCurrentDirection();
        secureDirection = snake.getSecureDirection();
        this.component = component;
    }

    /**
     * Method that overrides Ai Move.
     */
    @Override
    public void aiMove() {
        if (checkIfChangeIsPossible()) {
            changeDirection(findDirection());
        }
        checkBorders();
        checkObstacles();
        this.frogMove();
    }

    /**
     * Method that overrides draw method.
     * @param g
     */
    @Override
    public void draw(Graphics g) {
        ImageIcon icon = new ImageIcon("images/frog.png");
        icon.paintIcon(component, g, coordinates.get(0), coordinates.get(1));

    }

    /**
     * Method that changes coordinates after being caught.
     */
    public void changeCoordinatesAfterBeingCaught(){

        coordinates.set(0,360);
        coordinates.set(1,360);
    }

    /**
     * Method that checks if direction change is possible.
     * @return true or false
     */
    public boolean checkIfChangeIsPossible() {
        if (coordinates.get(0) == 0 && findClosestSnake().getCurrentDirection() == 'L') {
            return false;
        } else if (coordinates.get(0) == WIDTH_WINDOW - SIZE_OF_UNIT && findClosestSnake().getCurrentDirection() == 'R') {
            return false;
        } else if (coordinates.get(1) == WIDTH_WINDOW - SIZE_OF_UNIT && findClosestSnake().getCurrentDirection() == 'D') {
            return false;
        } else return coordinates.get(1) != 0 || findClosestSnake().getCurrentDirection() != 'U';
    }

    /**
     * Method that finds closest snake.
     * @return closest snake
     */
    public Snake findClosestSnake() {
        Snake closestSnake = new Snake(0, 0, null, null);
        List<Snake> snakes = new ArrayList<>();
        snakes.add(snake);
        snakes.add(aiSnake);
        int minDistance = 100000;
        for (Snake snake : snakes) {
            int distance = (int) Math.sqrt(Math.pow(Math.abs(snake.getSnakeCoordinates().get(0).get(0) - coordinates.get(0)), 2) + Math.pow(Math.abs(snake.getSnakeCoordinates().get(0).get(1) - coordinates.get(1)), 2));
            if (distance < minDistance) {
                minDistance = distance;
                closestSnake = snake;
            }
        }
        return closestSnake;
    }

    /**
     * Method that checks obstacles.
     */
    public void checkObstacles() {
        List<List<Integer>> objectsToAvoid = new ArrayList<>(obstacle);
        objectsToAvoid.addAll(snake.getSnakeCoordinates());
        objectsToAvoid.addAll(aiSnake.getSnakeCoordinates());
        List<Integer> nextFrogPosition = new ArrayList<>();
        switch (currentDirection) {
            case 'R':
                if (secureDirection == 'R') {
                    nextFrogPosition.add(coordinates.get(0) + SIZE_OF_UNIT);
                    nextFrogPosition.add(coordinates.get(1));
                    if (objectsToAvoid.contains(nextFrogPosition)) {
                        changeDirection(chooseDirectionInY());
                    }
                }
                break;
            case 'L':
                if (secureDirection == 'L') {
                    nextFrogPosition.add(coordinates.get(0) - SIZE_OF_UNIT);
                    nextFrogPosition.add(coordinates.get(1));
                    if (objectsToAvoid.contains(nextFrogPosition)) {
                        changeDirection(chooseDirectionInY());
                    }
                }
                break;
            case 'D':
                if (secureDirection == 'D') {
                    nextFrogPosition.add(coordinates.get(0));
                    nextFrogPosition.add(coordinates.get(1) + SIZE_OF_UNIT);
                    if (objectsToAvoid.contains(nextFrogPosition)) {
                        changeDirection(chooseDirectionInX());
                    }
                }
                break;
            case 'U':
                if (secureDirection == 'U') {
                    nextFrogPosition.add(coordinates.get(0));
                    nextFrogPosition.add(coordinates.get(1) - SIZE_OF_UNIT);
                    if (objectsToAvoid.contains(nextFrogPosition)) {
                        changeDirection(chooseDirectionInX());
                    }
                }
                break;
        }
    }

    /**
     * Method that changes direction into x.
     * @return direction
     */
    private char chooseDirectionInX() {
        if (coordinates.get(0) < WIDTH_WINDOW / 2) {
            return 'R';
        } else {
            return 'L';
        }
    }

    /**
     * Method that changes direction into y.
     * @return direction
     */
    private char chooseDirectionInY() {
        if (coordinates.get(1) < WIDTH_WINDOW / 2) {
            return 'D';
        } else {
            return 'U';
        }
    }

    /**
     * Method that changes direction.
     * @param wantedDirection
     */
    public void changeDirection(char wantedDirection) {

        switch (wantedDirection) {
            case 'R':
                if (currentDirection != 'L' && secureDirection != 'L' && ifDirectionCanBeChanged(wantedDirection)) {
                    currentDirection = 'R';
                }
                break;
            case 'L':
                if (currentDirection != 'R' && secureDirection != 'R' && ifDirectionCanBeChanged(wantedDirection)) {
                    currentDirection = 'L';
                }
                break;
            case 'D':
                if (currentDirection != 'U' && secureDirection != 'U' && ifDirectionCanBeChanged(wantedDirection)) {
                    currentDirection = 'D';
                }
                break;
            case 'U':
                if (currentDirection != 'D' && secureDirection != 'D' && ifDirectionCanBeChanged(wantedDirection)) {
                    currentDirection = 'U';

                }
                break;
        }
    }

    /**
     * Method that checks if direction can be changed.
     * @param direction checked direction
     * @return true or false
     */
    public boolean ifDirectionCanBeChanged(char direction) {

        List<List<Integer>> objectsToAvoid = new ArrayList<>(obstacle);

        objectsToAvoid.addAll(snake.getSnakeCoordinates());
        objectsToAvoid.addAll(aiSnake.getSnakeCoordinates());
        boolean result = false;
        List<Integer> nextFrogPosition = new ArrayList<>();
        switch (direction) {
            case 'R' -> {
                nextFrogPosition.add(coordinates.get(0) + SIZE_OF_UNIT);
                nextFrogPosition.add(coordinates.get(1));
                result = !objectsToAvoid.contains(nextFrogPosition);
            }
            case 'L' -> {
                nextFrogPosition.add(coordinates.get(0) - SIZE_OF_UNIT);
                nextFrogPosition.add(coordinates.get(1));
                result = !objectsToAvoid.contains(nextFrogPosition);
            }
            case 'D' -> {
                nextFrogPosition.add(coordinates.get(0));
                nextFrogPosition.add(coordinates.get(1) + SIZE_OF_UNIT);
                result = !objectsToAvoid.contains(nextFrogPosition);
            }
            case 'U' -> {
                nextFrogPosition.add(coordinates.get(0));
                nextFrogPosition.add(coordinates.get(1) - SIZE_OF_UNIT);
                result = !objectsToAvoid.contains(nextFrogPosition);
            }
        }
        return result;
    }

    /**
     * Method that checks borders.
     */
    public void checkBorders() {
        //System.out.println(coordinates);
        switch (currentDirection) {
            case 'L':
                if (coordinates.get(0) <= 0) {
                    changeDirection(chooseDirectionInY());
                }
                break;
            case 'R':
                if (coordinates.get(0) >= WIDTH_WINDOW - SIZE_OF_UNIT) {
                    changeDirection(chooseDirectionInY());
                }
                break;
            case 'U':
                if (coordinates.get(1) <= 0) {
                    changeDirection(chooseDirectionInX());
                }
                break;
            case 'D':
                if (coordinates.get(1) >= HEIGHT_WINDOW - SIZE_OF_UNIT) {
                    changeDirection(chooseDirectionInX());
                }
                break;
        }
    }

    /**
     * Method that finds a direction.
     * @return
     */
    public char findDirection() {

        Snake closestSnake = findClosestSnake();
        return closestSnake.getCurrentDirection();
    }

    /**
     * Method that moves a frog.
     */
    public void frogMove() {
        //to prevent multiclicking buttons and possibility error
        secureDirection = currentDirection;

        switch (currentDirection) {
            case 'R' -> coordinates.set(0, (coordinates.get(0) + SIZE_OF_UNIT));
            case 'L' -> coordinates.set(0, (coordinates.get(0) - SIZE_OF_UNIT));
            case 'U' -> coordinates.set(1, (coordinates.get(1) - SIZE_OF_UNIT));
            case 'D' -> coordinates.set(1, (coordinates.get(1) + SIZE_OF_UNIT));
        }
    }
}
