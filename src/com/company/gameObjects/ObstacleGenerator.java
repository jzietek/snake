package com.company.gameObjects;

import com.company.interfaces.Drawable;

import java.awt.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.company.gameScreens.GamePanel.*;

/**
 * Obstacle generator's class that is responsible for creating obstacles.
 */
public class ObstacleGenerator implements Drawable {

    /**
     * A list of obstacles.
     */
    List<List<Integer>> obstacles;

    /**
     * A constructor that creates obstacles.
     */
    public ObstacleGenerator(){

        generateObstacle();
    }

    /**
     * Method that generates obstacle.
     */
    private void generateObstacle(){
        obstacles = new ArrayList<>();
        char[] directions = {'R', 'L', 'D', 'U'};
        Random random = new Random();
        int numberUnitsFromBorderWindow = 16;
        int xStartObstaclePosition = (random.nextInt(WIDTH_WINDOW / SIZE_OF_UNIT - numberUnitsFromBorderWindow) + numberUnitsFromBorderWindow / 2) * SIZE_OF_UNIT;
        int yStartObstaclePosition = (random.nextInt(HEIGHT_WINDOW / SIZE_OF_UNIT - numberUnitsFromBorderWindow) + numberUnitsFromBorderWindow / 2) * SIZE_OF_UNIT;
        ArrayList<Integer> startObstaclePosition = new ArrayList<>();
        startObstaclePosition.add(xStartObstaclePosition);
        startObstaclePosition.add(yStartObstaclePosition);
        obstacles.add(startObstaclePosition);
        char direction = directions[random.nextInt(4)];
        int minAmount = 12;
        int maxAmount = 30;
        int length = random.nextInt(maxAmount) + minAmount;
        for (int i = 1; i < length; i++) {
            ArrayList<Integer> obstaclePosition = new ArrayList<>();
            switch (direction) {
                case 'R':
                    if (xStartObstaclePosition + i * SIZE_OF_UNIT < WIDTH_WINDOW - SIZE_OF_UNIT) {
                        obstaclePosition.add(xStartObstaclePosition + i * SIZE_OF_UNIT);
                        obstaclePosition.add(yStartObstaclePosition);
                        obstacles.add(obstaclePosition);
                    }
                    break;
                case 'L':
                    if (xStartObstaclePosition - i * SIZE_OF_UNIT > 0) {
                        obstaclePosition.add(xStartObstaclePosition - i * SIZE_OF_UNIT);
                        obstaclePosition.add(yStartObstaclePosition);
                        obstacles.add(obstaclePosition);
                    }
                    break;
                case 'D':
                    if (yStartObstaclePosition + i * SIZE_OF_UNIT < HEIGHT_WINDOW - SIZE_OF_UNIT) {
                        obstaclePosition.add(xStartObstaclePosition);
                        obstaclePosition.add(yStartObstaclePosition + i * SIZE_OF_UNIT);
                        obstacles.add(obstaclePosition);
                    }
                    break;
                case 'U':
                    if (yStartObstaclePosition - i * SIZE_OF_UNIT > 0) {
                        obstaclePosition.add(xStartObstaclePosition);
                        obstaclePosition.add(yStartObstaclePosition - i * SIZE_OF_UNIT);
                        obstacles.add(obstaclePosition);
                    }
                    break;
            }
        }
    }

    /**
     * Method that gets obstacles coordinates.
     * @return coordinates
     */
    public List<List<Integer>> getObstacles() {
        return obstacles;
    }

    /**
     * Method that overrides draw method.
     * @param g
     */
    @Override
    public void draw(Graphics g) {
        g.setColor(Color.black);
        obstacles.forEach(obstacle -> g.fillRect(obstacle.get(0), obstacle.get(1), SIZE_OF_UNIT, SIZE_OF_UNIT));
    }
}
