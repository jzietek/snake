package com.company;

import com.company.gameObjects.snake.AISnake;

/**
 * Ai snake runner class.
 */
public class AiSnakeRunner extends Thread {

    /**
     * Ai snake.
     */
    private AISnake aiSnake;

    /**
     * A constructor that creates an object.
     * @param aiSnake
     */
    public AiSnakeRunner(AISnake aiSnake) {

        this.aiSnake = aiSnake;

    }

    /**
     * Method that overrides run method.
     */
    @Override
    public void run() {
        aiSnake.aiMove();
    }
}