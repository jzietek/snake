package com.company.interfaces;

/**
 * Ai mode interface.
 */
public interface AIMode {

    /**
     * A ai move method.
     */
    public void aiMove();
}
