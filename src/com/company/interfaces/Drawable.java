package com.company.interfaces;

import java.awt.*;

/**
 * Drawable interface.
 */
public interface Drawable {

    /**
     * A draw method.
     * @param g
     */
    public void draw(Graphics g);
}
